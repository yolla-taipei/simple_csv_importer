<?php

namespace Drupal\simple_csv_importer\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\simple_csv_importer\SimpleCSVImporter;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SimpleCSVImporterForm extends FormBase {

  /** @var \Drupal\simple_csv_importer\SimpleCSVImporter */
  protected $importer;

  /**
   * SimpleCSVImporterForm constructor.
   *
   * @param \Drupal\simple_csv_importer\SimpleCSVImporter $importer
   */
  public function __construct(SimpleCSVImporter $importer) {
    $this->importer = $importer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('simple_csv_importer.importer')
    );
  }

  /**
   * @inheritdoc
   */
  public function getFormId() {
    return 'simple_csv_importer';
  }

  /**
   * @inheritdoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['file'] = [
      '#type' => 'managed_file',
      '#title' => t('Import CSV File'),
      '#size' => 40,
      '#description' => $this->t('Select the CSV file to be imported. There must be a header row, and the headers should represent fields to map values to on the created entity.'),
      '#required' => TRUE,
      '#upload_location' => 'private://simple-csv-importer/',
      '#autoupload' => TRUE,
      '#upload_validators' => ['file_validate_extensions' => ['csv']],
    ];

    $form['entity_type'] = [
      // @todo
      '#type' => 'value',
      '#value' => 'node',
    ];

    $form['bundle'] = [
      // @todo
      '#type' => 'value',
      '#value' => 'review',
    ];

    $form['unique_key'] = [
      '#type' => 'value',
      '#value' => NULL,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * @inheritdoc
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $file = $form_state->getValue('file');
    $file = File::load($file[0]);

    if (!$file) {
      $form_state->setError($form['file'], $this->t('The file could not be uploaded.'));
      return;
    }

    $contents = file_get_contents($file->getFileUri());
    $encoding = mb_detect_encoding($contents, 'UTF-8', true);

    if ($encoding !== 'UTF-8') {
      $test_encodings = ['ASCII', 'Windows-1252'];
      $encoding_resolved = false;

      // Try to transliterate characters
      foreach ($test_encodings as $encoding) {
        if (mb_check_encoding($contents, $encoding)) {
          // If iconv fails, use mb_convert_encoding anyway
          $contents = iconv($encoding, 'UTF-8', $contents) ?: mb_convert_encoding($contents, 'UTF-8');
          $encoding_resolved = true;
          break;
        }
      }

      if (!$encoding_resolved) {
        // Convert the encoding without preservation.
        $contents = mb_convert_encoding($contents, 'UTF-8');
      }

      file_put_contents($file->getFileUri(), $contents);
      $this->messenger()->addWarning($this->t('CSV file was automatically converted to UTF-8 character encoding.'));
    }

    try {
      $this->importer->test($file->getFileUri());
    } catch (\League\Csv\Exception $e) {
      $form_state->setError($form['file'], $e->getMessage());
    }
  }

  /**
   * @inheritdoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $file = $form_state->getValue('file');
    $file = File::load($file[0]);
    $file->setPermanent();
    $file->save();

    $context = $form_state->getValue('context');

    // Give modules an opportunity to alter the context based on the submission
    \Drupal::moduleHandler()->alter('simple_csv_importer_context', $context, $form, $form_state);

    $this->importer->import(
      $file->getFileUri(),
      $form_state->getValue('entity_type'),
      $form_state->getValue('bundle'),
      $form_state->getValue('unique_key'),
      $context
    );
  }
}
