Simple CSV Importer for Drupal 8
================================

Installation with Composer
--------------------------

Add the following to the repositories section of composer.json

    {
        "type": "vcs",
        "url": "git@bitbucket.org:yolla-taipei/simple_csv_importer.git"
    }

Then install with Composer using:

    composer require yolla-taipei/simple_csv_importer
